import socket
from _thread import *
import sys

#server = "192.168.1.101"
server = "169.254.71.124"
port = 5555

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.bind((server, port))
except socket.error as e:
    str(e)

s.listen()
print("Waiting for a connection, Server Started")

def threaded_client(conn):
    reply = ""
    while True:
        try:
            data = conn.recv(2048)
            reply = data.decode("utf-8")
            
            if data == "exit":
                conn.sendall(str.encode(reply))
                s.shutdown()
                s.close()
                break

            if not data:
                print("Disconnected")
                break
            else:
                print("Recieved: ", reply)
                print("Sending: ", reply)
            

            conn.sendall(str.encode("WE GOT IT!!!"))
        except:
            break

while True:
   conn, addr = s.accept()
   print("Connected to:", addr)

   start_new_thread(threaded_client, (conn,))